# Summary

[About](about.md)

[Reducers in react](basic-reducer-in-react.md)
[A bag of rails tips](bag-of-rails-tips.md)
[Setting up Sid Development Environment for Debian Packaging](creating-a-sid-env.md)
[Using GnuPG](gnupg.md)
[Simple Packaging Tutorial: The Long Version](simple-packaging-tutorial.md)
[Updating a Debian Package](updating-a-debian-package.md)
[Zsh with Oh My Zsh](getting-started-with-zsh.md)
[My Laptop](my-laptop.md)
[Software I Use](software-i-use.md)
